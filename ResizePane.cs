﻿using System.Diagnostics.CodeAnalysis;
using Unity.Collections.LowLevel.Unsafe;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UIElements;
using Cursor = UnityEngine.UIElements.Cursor;

namespace EntityDebuggerUIE
{
    public class ResizePane : VisualElement
    {
        public new class UxmlFactory : UxmlFactory<ResizePane, UxmlTraits>
        {
        }

        private VisualElement _leftSystems, _rightEntities;

        public void Create()
        {
            var dragLine = this.Q<VisualElement>("DragLineAnchor");
            Assert.IsNotNull(dragLine);

            var horizontal = new CursorCopy { defaultCursorId = (int)MouseCursor.ResizeHorizontal };
            dragLine.style.cursor = new(UnsafeUtility.As<CursorCopy, Cursor>(ref horizontal));
            dragLine.AddManipulator(new Resizer());
        }

        #region System Resizer

        private class Resizer : MouseManipulator
        {
            private Vector2 _start;
            private bool    _active;

            public Resizer()
            {
                activators.Add(new() { button = MouseButton.LeftMouse });
                _active = false;
            }

            protected override void RegisterCallbacksOnTarget()
            {
                target.RegisterCallback<MouseDownEvent>(OnMouseDown);
                target.RegisterCallback<MouseMoveEvent>(OnMouseMove);
                target.RegisterCallback<MouseUpEvent>(OnMouseUp);
            }

            protected override void UnregisterCallbacksFromTarget()
            {
                target.UnregisterCallback<MouseDownEvent>(OnMouseDown);
                target.UnregisterCallback<MouseMoveEvent>(OnMouseMove);
                target.UnregisterCallback<MouseUpEvent>(OnMouseUp);
            }

            private void OnMouseDown(MouseDownEvent e)
            {
                if (_active)
                {
                    e.StopImmediatePropagation();
                    return;
                }

                if (CanStartManipulation(e))
                {
                    _start = e.localMousePosition;

                    _active = true;
                    target.CaptureMouse();
                    e.StopPropagation();
                }
            }

            private void OnMouseMove(MouseMoveEvent e)
            {
                if (!_active || !target.HasMouseCapture())
                    return;

                Vector2 diff = e.localMousePosition - _start;

                target.parent.style.width = target.parent.layout.width + diff.x;

                e.StopPropagation();
            }

            private void OnMouseUp(MouseUpEvent e)
            {
                if (!_active || !target.HasMouseCapture() || !CanStopManipulation(e))
                    return;

                _active = false;
                target.ReleaseMouse();
                e.StopPropagation();
            }
        }

        #endregion

        #region Cursor

        /// <summary>
        ///     Direct copy of UnityEngine.UIElements.Cursor
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        private struct CursorCopy
        {
            public Texture2D texture { get; set; }
            public Vector2 hotspot { get; set; }

            // ReSharper disable once UnusedAutoPropertyAccessor.Local
            public int defaultCursorId { get; set; }
        }

        #endregion
    }
}
