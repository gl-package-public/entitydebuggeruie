﻿using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace EntityDebuggerUIE
{
    public class EntitySet : ToolbarMenu
    {
        public new class UxmlFactory : UxmlFactory<EntitySet, UxmlTraits>
        {
        }

        private const string ShowSingle = "Show Singletons",
            CommandBuffer               = "Command Buffers",
            Systems                     = "System Entities",
            Prefabs                     = "Entity Prefabs",
            Scenes                      = "Subscenes";

        public bool ShowCommands, ShowSystems, ShowScenes, ShowPrefabs = true;

        public void Update()
        {
            // * Skip if cached items exist.
            if (menu.MenuItems().Count > 0)
                return;

            // * Refresh the dropdown options.
            menu.AppendAction(ShowSingle, null, DropdownMenuAction.Status.Disabled);
            menu.AppendAction(CommandBuffer, _ => SetToggle(ref ShowCommands),
                ShowCommands ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
            menu.AppendAction(Systems, _ => SetToggle(ref ShowSystems),
                ShowSystems ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
            menu.AppendAction(Prefabs, _ => SetToggle(ref ShowPrefabs),
                ShowPrefabs ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
            menu.AppendAction(Scenes, _ => SetToggle(ref ShowScenes),
                ShowScenes ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
        }

        private void SetToggle(ref bool toggle)
        {
            // * Invert state of toggle.
            // ? Can't use a method for the entire append action because this is a ref.
            toggle = !toggle;

            // * Regenerate the entity graph.
            EntityDebuggerV2.Instance.ForceRefresh   = true;
            EntityDebuggerV2.Instance.EntListChanged = true;

            // * Refresh to update check mark.
            menu.ClearItems();
        }
    }
}
