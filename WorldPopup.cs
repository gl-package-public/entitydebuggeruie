﻿using Unity.Entities;
using Unity.Entities.Editor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace EntityDebuggerUIE
{
    public class WorldPopup : ToolbarMenu
    {
        public new class UxmlFactory : UxmlFactory<WorldPopup, UxmlTraits>
        {
        }

        private const string ShowFullPlayerLoop = "Show Full Player Loop";
        private const string ShowAllWorlds      = "Show All Worlds";
        private const string NoWorldsAvailable  = "No Worlds";

        private readonly WorldListChangeTracker _worldChange = new();

        /// <summary>
        ///     Forces world selection to default to 0.
        /// </summary>
        private bool _resetSelection;

        /// <summary>
        ///     World selection options.
        /// </summary>
        private bool ShowPlayerLoop => Selected is null;

        /// <summary>
        ///     Matches the string name of the world previously displayed.
        /// </summary>
        private string _prevWorld;

        /// <summary>
        /// Quick access to selected world.
        /// </summary>
        private World Selected
        {
            get => EntityDebuggerV2.SelectedWorld;
            set
            {
                // * Cache the selected name in case of rebuild.
                _prevWorld = value is null ? string.Empty : value.Name;

                EntityDebuggerV2.SelectedWorld = value;
            }
        }

        public void Update()
        {
            // * Check if number of worlds has changed.
            if (_worldChange.HasChanged())
            {
                // * Quantity of dropdown menu will determine if needing to be rebuilt.
                menu.ClearItems();

                // * Reset world selection. This also triggers world changed.
                Selected = null;

                _resetSelection = true;

                // * Also trigger the main proxy manager to rebuild.
                EntityDebuggerV2.ProxyManager.CreateWorldProxiesForAllWorlds();
            }

            if (EntityDebuggerV2.Instance.WorldChanged)
                // * Also force a refresh of the world list.
                menu.ClearItems();

            AttemptPreviousSelection();

            EntityDebuggerV2.ProxyManager.IsFullPlayerLoop = ShowPlayerLoop;

            text = ShowPlayerLoop ? ShowFullPlayerLoop : _prevWorld;

            PopulateDropdown();
        }

        private void PopulateDropdown()
        {
            // * Cached list already exists, skip.
            if (menu.MenuItems().Count > 0)
                return;

            foreach (World world in World.All)
            {
                if (!Utilities.WorldFilter(world))
                    continue;

                menu.AppendAction(world.Name, _ =>
                {
                    // * Reassign targeted world.
                    Selected = world;
                }, Selected == world ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
            }

            // * No worlds found!
            if (menu.MenuItems().Count == 0)
                menu.AppendAction(NoWorldsAvailable, _ => { }, DropdownMenuAction.Status.Disabled);

            menu.AppendSeparator();
            // * Show all worlds toggle.
            menu.AppendAction(ShowAllWorlds, _ =>
            {
                // * Toggle boolean.
                EntityDebuggerV2.Instance.ShowAllWorlds = !EntityDebuggerV2.Instance.ShowAllWorlds;

                // * Entity list needs to be redrawn in order to list all worlds.
                EntityDebuggerV2.Instance.EntListChanged = true;

                // * Force refresh of list.
                menu.ClearItems();
            }, EntityDebuggerV2.Instance.ShowAllWorlds
                ? DropdownMenuAction.Status.Checked
                : DropdownMenuAction.Status.Normal);
            menu.AppendSeparator();
            menu.AppendAction(ShowFullPlayerLoop, _ =>
            {
                // * Designate full player loop.
                Selected = null;

                // * Force refresh of list.
                menu.ClearItems();
            }, ShowPlayerLoop ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
        }

        private void AttemptPreviousSelection()
        {
            // * No Worlds, just early return.
            if (World.All.Count == 0)
                return;

            if (_resetSelection)
            {
                // * Default world to first in list if nothing previously selected.
                Selected = World.All[0];

                _resetSelection = false;
                return;
            }

            // * Showing player loop overrides world selection with null.
            // ? Which will show all worlds and all systems.
            if (ShowPlayerLoop)
                return;

            if (_prevWorld != Selected.Name)
                foreach (World world in World.All)
                {
                    // * Iterating through all worlds to find equal name.
                    if (_prevWorld != world.Name)
                        continue;

                    Selected = world;
                    return;
                }
        }
    }
}
