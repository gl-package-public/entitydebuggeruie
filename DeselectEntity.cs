using Unity;
using Unity.Entities.Editor;
using UnityEditor;

namespace EntityDebuggerUIE
{
    [InitializeOnLoad]
    public static class DeselectEntity
    {
        static DeselectEntity()
        {
            // * Fixing unity's entity inspector bugs by deselecting on reload.
            EditorApplication.playModeStateChanged += DeselectPlayMode;
        }

        private static void DeselectPlayMode(PlayModeStateChange state)
        {
            if (Selection.activeObject is EntitySelectionProxy)
                Selection.activeObject = null;
        }
    }
}
