﻿using System.Collections.Generic;
using System.Linq;
using EntityDebuggerUIE.EntityElements;
using Unity.Collections;
using Unity.Editor.Bridge;
using Unity.Entities;
using Unity.Entities.Editor;
using Unity.NetCode;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UIElements;
using TreeViewItemData = UnityEngine.UIElements.TreeViewItemData<EntityDebuggerUIE.EntityList.EntityTreeViewItem>;

namespace EntityDebuggerUIE
{
    public class EntityList
    {
        private readonly MultiColumnTreeView _entityView;

        private readonly EntitySet  _settings;

        private readonly Label _counter;

        /// <summary>
        ///     Array of all relevant entities with structural changes.
        ///     Usually just [1] except when player loop is selected then it's all worlds.
        /// </summary>
        private readonly List<ListQuery> _queries = new();

        // * Must be public for use in the compiler using statements.
        public class EntityTreeViewItem
        {
            // * Data being listed.
            public readonly string Name;

            // * For USS icon rendering.
            public readonly EntityType Type;

            // * For selection targeting.
            public readonly World  World;
            public readonly Entity Entity;

            // * For chunk targeting.
            public readonly ulong ArchHash;

            public EntityTreeViewItem(string name, EntityType type, World world, Entity entity, ulong archHash)
            {
                Name     = name;
                Type     = type;
                World    = world;
                Entity   = entity;
                ArchHash = archHash;
            }
        }

        /// <summary>
        ///     CSS class names for entity icon and coloring.
        /// </summary>
        private const string
            Index     = "entity-index",
            WorldName = "entity-world",
            Chunk     = "entity-chunk",
            Regular   = "entity-regular",
            Prefab    = "entity-prefab",
            Ghost     = "entity-ghost";

        public enum EntityType
        {
            World,
            Chunk,
            Regular,
            Prefab,
            Ghost
        }

        public EntityList(MultiColumnTreeView entView, EntitySet settings, Label counter)
        {
            _entityView = entView;
            _settings   = settings;
            _counter    = counter;

            // ! HARDCODED! Ensure matches with the UXML file.
            Column index  = _entityView.columns["Index"];
            Column entity = _entityView.columns["Entities"];

            // * No unbinding callback required. Nothing is being recorded.
            index.makeCell = () =>
            {
                var label = new Label();
                label.AddToClassList(Index);
                return label;
            };
            index.bindCell = (element, i) =>
            {
                var label = (Label)element;
                var data  = _entityView.GetItemDataForIndex<EntityTreeViewItem>(i);

                // * Categories do not have indices.
                label.SetVisibility(data.Type != EntityType.World && data.Type != EntityType.Chunk);

                label.text = data.Entity.Index.ToString();
            };

            entity.makeCell = () => new EntityLabel();
            entity.bindCell = (element, i) =>
            {
                var label = (EntityLabel)element;
                var data  = _entityView.GetItemDataForIndex<EntityTreeViewItem>(i);

                // * Remove old classes.
                label.RemoveFromClassList(WorldName);
                label.RemoveFromClassList(Chunk);
                label.RemoveFromClassList(Regular);
                label.RemoveFromClassList(Prefab);
                label.RemoveFromClassList(Ghost);

                // * Add icon classes.
                switch (data.Type)
                {
                    case EntityType.World:
                        label.AddToClassList(WorldName);
                        break;
                    case EntityType.Chunk:
                        label.AddToClassList(Chunk);
                        break;
                    case EntityType.Regular:
                        label.AddToClassList(Regular);
                        break;
                    case EntityType.Prefab:
                        label.AddToClassList(Prefab);
                        break;
                    case EntityType.Ghost:
                        label.AddToClassList(Ghost);
                        break;
                }

                // * Set the name.
                label.Text = data.Name;
            };

            _entityView.selectionChanged += _ =>
            {
                if (_entityView.selectedItem is not EntityTreeViewItem selected)
                    return;

                // * All rows except for world selected using the entity list window will highlight a chunk.
                if (selected.Type is EntityType.Chunk)
                    EntityDebuggerV2.SelectedChunkHash = selected.ArchHash;

                if (selected.Type is EntityType.Chunk or EntityType.World)
                    // * Just highlight and early return.
                    return;

                // * Skip selecting null entities.
                if (selected.Entity == Entity.Null)
                    return;

                EntityDebuggerV2.SelectedEntity = EntitySelectionProxy.CreateInstance(selected.World, selected.Entity);
                SetSelection();
            };
        }

        public void SetSelection()
        {
            int undoGroup = Undo.GetCurrentGroup();

            EntitySelectionProxy selected = EntityDebuggerV2.SelectedEntity;

            // * Entire reason why there's a complicated id calculation.
            // ? If there are multiple worlds, it needs to select the proper one using a merged world-index value.
            FindWorldIndex(selected.World, out int worldIdx);
            int id = GetElementId(worldIdx, selected.Entity.Index);
            _entityView.SetSelectionByIdWithoutNotify(new[] { id });
            _entityView.ScrollToItemById(id);

            EntityManager em = selected.World.EntityManager;

            // * Find archetype and set it as selected in the chunk list.
            EntityDebuggerV2.SelectedChunkHash = GetArchHash(em, selected.Entity);

            Object author = em.Debug.GetAuthoringObjectForEntity(selected.Entity);
            if (author is null)
                selected.Select();
            else
                // Selected entities should always try to show up in Runtime mode
                SelectionBridge.SetSelection(author, selected, DataMode.Runtime);

            Undo.CollapseUndoOperations(undoGroup);
        }

        private static unsafe ulong GetArchHash(EntityManager em, Entity ent)
        {
            // * Kinda redundant check but the next line is a bunch of unsafe accesses with no bounds checking.
            if (ent == Entity.Null || ent.Index < 0)
                return 0;

            // * Pulling out archetype hash from deep within Entities.
            // ? Why they didn't expose this as a possibility is beyond me.
            return em.GetCheckedEntityDataAccess()->EntityComponentStore->GetArchetype(ent)->StableHash;
        }

        public void Update()
        {
            if (EntityDebuggerV2.Instance.WorldChanged || EntityDebuggerV2.Instance.SystemChanged ||
                EntityDebuggerV2.Instance.EntListChanged)
                Utilities.ResetQueries(_queries, _settings);

            if (_queries.Count == 0)
            {
                // * No queries, no worlds. Clear the list.
                SetCounter(0);
                _entityView.SetRootItems(new List<TreeViewItemData>());
                _entityView.Rebuild();
                return;
            }

            // * Structural change filter.
            if (Utilities.DidNotChange(_queries) && !EntityDebuggerV2.Instance.ArchChanged)
                return;

            //Debug.Log("Rebuilding entity list.");

            RebuildList();
        }

        private unsafe void RebuildList()
        {
            // * Rebuilding entirety of the list.
            var entities = new List<TreeViewItemData>();

            // * Counter is for total entities.
            // * Magic is for items that are not selectable.
            int counter = 0, magic = 0;
            foreach (ListQuery item in _queries)
            {
                // * Caching world index.
                World world = item.Manager.World;
                FindWorldIndex(world, out int worldIdx);

                using NativeArray<ArchetypeChunk> chunks = item.Query.ToArchetypeChunkArray(Allocator.Temp);

                // * Pointer access.
                EntityTypeHandle entType = item.Manager.GetEntityTypeHandle();

                var queryCount = 0;

                // * Populate the list with entities.
                var branch = new List<TreeViewItemData>();
                foreach (ArchetypeChunk chunk in chunks)
                {
                    ulong stableHash = chunk.Archetype.StableHash;

                    // * Filter by archetype.
                    if (EntityDebuggerV2.SelectedArchHash is not 0 && EntityDebuggerV2.SelectedArchHash != stableHash)
                        continue;

                    // * Increment counter.
                    queryCount += chunk.Count;

                    bool isPrefab = chunk.Has<Prefab>();
#if UNITY_NETCODE
                    bool isGhost = chunk.Has<GhostType>();
#endif
                    var leaf = new List<TreeViewItemData>();

                    Entity* ent = chunk.GetEntityDataPtrRO(entType);
                    for (var i = 0; i < chunk.Count; i++)
                    {
                        string name = item.Manager.GetName(ent[i]);
                        name = string.IsNullOrWhiteSpace(name) ? ent[i].ToString() : name;

                        // * For CSS icons.
                        var type = EntityType.Regular;
                        if (isPrefab)
                            type = EntityType.Prefab;
#if UNITY_NETCODE
                        else if (isGhost)
                            type = EntityType.Ghost;
#endif

                        leaf.Add(new(GetElementId(worldIdx, ent[i].Index),
                            new(name, type, world, ent[i], stableHash)));
                    }

                    // * Sort the data by index.
                    leaf.Sort((data, itemData) => data.data.Entity.Index.CompareTo(itemData.data.Entity.Index));

                    branch.Add(new(magic++, new($"Chunk ({stableHash})", EntityType.Chunk,
                        world, Entity.Null, stableHash), leaf));
                }

                // * Sort chunks by first entity index.
                // ? Entities are already sorted by index within each branches so this should result in a completely sorted list.
                branch.Sort((left, right) =>
                    left.children.First().data.Entity.Index.CompareTo(right.children.First().data.Entity.Index));

                if (EntityDebuggerV2.SelectedWorld is not null)
                {
                    // * If there's a selected world, no nesting.
                    entities = branch;
                    // * Also display the count on the upper right.
                    counter = queryCount;
                }
                else
                {
                    // * Otherwise, nest by world and include the count.
                    entities.Add(new(magic++, new($"{world.Name} ({queryCount})",
                        EntityType.World, world, Entity.Null, 0), branch));
                    counter += queryCount;
                }
            }

            SetCounter(counter);

            _entityView.SetRootItems(entities);
            _entityView.RefreshItems();
            _entityView.ExpandAll();
        }

        /// <summary>
        ///     Set the match entities label value.
        /// </summary>
        /// <param name="value">Number to append to end of label.</param>
        private void SetCounter(int value)
        {
            _counter.text = $"Matched entities: {value}";
        }

        /// <summary>
        ///     Compute packed element index for O(1) selection.
        /// </summary>
        /// <param name="world">Index of world in filtered World.All list.</param>
        /// <param name="entityIdx">The index property of the entity.</param>
        /// <returns>Packed int that can be used to select in this entity list. </returns>
        private static int GetElementId(int world, int entityIdx)
        {
            // * Max 63 worlds. World 0 is reserved for magic numbers.
            Assert.IsTrue(world + 1 < 1 << 6);

            // * In order to fit within available bits, must be less than 16,777,216
            Assert.IsTrue(entityIdx < 1 << 24);

            // * First 6 bits are for world index for a max of 64 worlds.
            return ((world + 1) << 24) | entityIdx;
        }

        /// <summary>
        ///     Iterate through all worlds and compute index of target.
        /// </summary>
        /// <param name="world">Target world.</param>
        /// <param name="worldIdx">Resulting world index.</param>
        private static void FindWorldIndex(World world, out int worldIdx)
        {
            // * Find the index of this world in all worlds list.
            worldIdx = 0;
            foreach (World target in World.All)
            {
                if (!Utilities.WorldFilter(target))
                    continue;

                if (target == world)
                    break;

                worldIdx++;
            }
        }
    }
}
