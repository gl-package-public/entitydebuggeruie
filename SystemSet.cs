﻿using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace EntityDebuggerUIE
{
    public class SystemSet : ToolbarMenu
    {
        public new class UxmlFactory : UxmlFactory<SystemSet, UxmlTraits>
        {
        }

        private const string ViewOptions = "View Options",
            Precision                    = "Precise Time",
            Reordering                   = "Allow Reordering",
            Resizing                     = "Allow Resizing";

        /// <summary>
        ///     Include 2 more digits in the system timing.
        /// </summary>
        public bool MorePrecision;

        public void Update(MultiColumnTreeView systems)
        {
            // * Skip if cached items exist.
            if (menu.MenuItems().Count > 0)
                return;

            // * Regenerate view options.
            menu.AppendAction(ViewOptions, null, DropdownMenuAction.Status.Disabled);
            menu.AppendAction(Precision, _ =>
            {
                MorePrecision = !MorePrecision;

                // * Regenerate the system graph.
                EntityDebuggerV2.Instance.ForceRefresh = true;

                // * Refresh to update check mark.
                menu.ClearItems();
            }, MorePrecision ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
            menu.AppendAction(Reordering, _ =>
            {
                systems.columns.reorderable = !systems.columns.reorderable;

                // * Refresh to update check mark.
                menu.ClearItems();
            }, systems.columns.reorderable ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
            menu.AppendAction(Resizing, _ =>
            {
                systems.columns.resizable = !systems.columns.resizable;

                // * Required to force dirty redraw and apply resizable settings.
                int last = systems.columns.Count - 1;
                systems.columns.ReorderDisplay(last, last);

                // * Refresh to update check mark.
                menu.ClearItems();
            }, systems.columns.resizable ? DropdownMenuAction.Status.Checked : DropdownMenuAction.Status.Normal);
        }
    }
}
