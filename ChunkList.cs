﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using EntityDebuggerUIE.EntityElements;
using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine.UIElements;

namespace EntityDebuggerUIE
{
    public class ChunkList
    {
        private readonly List<ListQuery> _queries = new();

        private readonly EntitySet _settings;

        private readonly TreeView _tree;

        private readonly List<ChunkItem> _items = new();

        public class ArchCounts
        {
            public readonly StringBuilder MaxStr, MaxVal, CntStr;

            public readonly int[] Bins;

            public int TotalCount;

            public float MaxCount;

            public ArchCounts(int capacity)
            {
                MaxStr = new();
                MaxVal = new();
                CntStr = new();
                Bins   = new int[capacity];
            }
        }

        private class MergedComps
        {
            public StringBuilder Name, Value;

            public string Class, Super;

            public ArchCounts Counts;

            public ulong Hash;

            public bool Ghost;
        }

        public ChunkList(TreeView tree, EntitySet settings)
        {
            _tree = tree;

            _settings = settings;

            tree.makeItem = () => new ChunkItem();
            tree.bindItem = (element, i) =>
            {
                var item = (ChunkItem)element;
                var data = tree.GetItemDataForIndex<MergedComps>(i);

                // * Clearing of previous class list done in unbind.
                element.AddToClassList(ChunkItem);
                element.AddToClassList(data.Super);
                element.AddToClassList(data.Class);

                item.Name.text  = data.Name?.ToString() ?? "";
                item.Value.text = data.Value?.ToString() ?? "";

                // * For graph creation.
                item.Graph.Counts = data.Counts;

                // * Get the actual background for each element.
                VisualElement background = item.parent.parent;

                // * Add ghost highlighting if component is networked.
                // ? This requires access to the toggle and indent which means it can't be place on element.
                background.RemoveFromClassList(CompGhost);
                if (data.Ghost)
                    background.AddToClassList(CompGhost);

                // * For group selection since this is all fragmented.
                item.Refresh = () =>
                {
                    background.RemoveFromClassList(Selected);
                    background.RemoveFromClassList(EntitySelected);
                    background.RemoveFromClassList(NotSelected);

                    if (EntityDebuggerV2.SelectedArchHash == data.Hash)
                        background.AddToClassList(Selected);
                    else if (EntityDebuggerV2.SelectedChunkHash == data.Hash)
                        background.AddToClassList(EntitySelected);
                    else
                        background.AddToClassList(NotSelected);
                };
                item.Refresh();

                _items.Add(item);
            };
            tree.unbindItem = (element, _) =>
            {
                element.ClearClassList();
                _items.Remove((ChunkItem)element);
            };

            tree.selectionChanged += _ =>
            {
                EntityDebuggerV2.SelectedArchHash = tree.selectedItem is not MergedComps selected ? 0 : selected.Hash;
                RefreshSelected();
            };
        }

        /// <summary>
        ///     CSS component component icon classes.
        /// </summary>
        private const string ChunkItem      = "chunk-item",
                             Selected       = "chunk-item-selected",
                             NotSelected    = "chunk-item-unselect",
                             EntitySelected = "chunk-item-entity-selected",
                             ArchCount      = "chunk-arch-count",
                             ArchCapacity   = "chunk-arch-capacity",
                             CompHeader     = "chunk-comp-header",
                             GraphicHeader  = "chunk-graph-header",
                             GraphicLabel   = "chunk-graph-label",
                             GraphicAxis    = "chunk-graph-axis",
                             GraphicMax     = "chunk-graph-max",
                             GraphicMin     = "chunk-graph-min",
                             Component      = "chunk-comp-icon",
                             CompReg        = "chunk-comp-regular",
                             CompBuffer     = "chunk-comp-buffer",
                             CompManaged    = "chunk-comp-managed",
                             CompTag        = "chunk-comp-tag",
                             CompShared     = "chunk-comp-shared",
                             CompChunk      = "chunk-comp-chunk",
                             CompGhost      = "chunk-comp-ghost";

        public void Update()
        {
            if (EntityDebuggerV2.Instance.WorldChanged || EntityDebuggerV2.Instance.EntityChanged ||
                EntityDebuggerV2.Instance.SystemChanged)
                Utilities.ResetQueries(_queries, _settings);

            if (_queries.Count == 0)
            {
                // * Clearing out old data.
                _tree.SetRootItems(new List<TreeViewItemData<bool>>());
                _tree.Rebuild();
                EntityDebuggerV2.SelectedArchHash = 0;
            }

            if (EntityDebuggerV2.Instance.ChunkChanged)
                RebuildList();

            // * Structural change filter.
            if (Utilities.DidNotChange(_queries))
                return;

            //Debug.Log("Rebuilding chunk list.");

            RebuildList();
            RefreshSelected();
        }

        private void RefreshSelected()
        {
            foreach (ChunkItem item in _items)
                item.Refresh();
        }

        private unsafe void RebuildList()
        {
            // * Record seen archetypes by type hash.
            var seen = new Dictionary<ulong, ArchCounts>();

            // * Just mash all the world queries together into one list view.
            List<TreeViewItemData<MergedComps>> items = new();

            var id = 0;
            foreach (ListQuery query in _queries)
            {
                using NativeArray<ArchetypeChunk> chunks = query.Query.ToArchetypeChunkArray(Allocator.Temp);
                foreach (ArchetypeChunk archetypeChunk in chunks)
                {
                    // * Get direct access to archetype as that contains important info.
                    ref readonly Archetype archetype = ref *archetypeChunk.Archetype.Archetype;

                    ulong stableHash = archetype.StableHash;

                    // * Check against stable hash as that can be shared across worlds.
                    var counts = new ArchCounts(archetype.ChunkCapacity);
                    if (!seen.TryAdd(stableHash, counts))
                    {
                        counts = seen[archetype.StableHash];
                        counts.Bins[archetypeChunk.Count - 1]++;
                        counts.TotalCount++;
                        continue;
                    }

                    // * Increment the counts.
                    counts.Bins[archetypeChunk.Count - 1]++;
                    counts.TotalCount++;

                    // * Breaking up the item because virtualization is pain.
                    items.Add(new(id++, new()
                    {
                        Name  = new("Chunk Count:"),
                        Value = counts.CntStr,
                        Class = ArchCount,
                        Hash  = stableHash
                    }));
                    items.Add(new(id++, new()
                    {
                        Name  = new("Chunk Capacity:"),
                        Value = new(archetype.ChunkCapacity.ToString()),
                        Class = ArchCapacity,
                        Hash  = stableHash
                    }));

                    // * Get all components from this.
                    var comps = new List<TreeViewItemData<MergedComps>>();
                    for (var i = 0; i < archetype.TypesCount; i++)
                    {
                        // * Type data from the archetype.
                        ref readonly TypeIndex type = ref archetype.Types[i].TypeIndex;

                        // * Pull managed type from index.
                        Type mType = TypeManager.GetType(type);

                        // * Initializing merged component data.
                        MergedComps comp = new()
                        {
                            Name  = new(mType.Name),
                            Value = new($"{archetype.SizeOfs[i]} B"),
                            Super = Component,
                            Hash  = stableHash
                        };

                        // * Hardcoded type cascade.
                        if (TypeManager.IsChunkComponent(type))
                            comp.Class = CompChunk;
                        else if (TypeManager.IsBuffer(type))
                            comp.Class = CompBuffer;
                        else if (TypeManager.IsSharedComponentType(type))
                            comp.Class = CompShared;
                        else if (TypeManager.IsManagedComponent(type))
                            comp.Class = CompManaged;
                        else if (TypeManager.IsZeroSized(type))
                            comp.Class = CompTag;
                        else
                            comp.Class = CompReg;

#if UNITY_NETCODE
                        // * Testing all fields for ghost component.
                        if (mType.IsDefined(typeof(GhostComponentAttribute), false))
                        {
                            comp.Ghost = true;
                        }
                        else if (mType.IsDefined(typeof(GhostEnabledBitAttribute), false))
                        {
                            comp.Ghost = true;
                        }
                        else if (mType.IsAssignableFrom(typeof(IInputComponentData)))
                        {
                            comp.Ghost = true;
                        }
                        else if (mType.IsAssignableFrom(typeof(ICommandData))) // ? Includes IInputBufferData
                        {
                            comp.Ghost = true;
                        }

                        // * Ghost components do not need the component attribute, just [GhostField].
                        else
                        {
                            FieldInfo[] fields = mType.GetFields();
                            foreach (FieldInfo field in fields)
                            {
                                if (!field.IsDefined(typeof(GhostFieldAttribute), false))
                                    continue;

                                comp.Ghost = true;
                                break;
                            }
                        }
#endif

                        comps.Add(new(id++, comp));
                    }

                    items.Add(new(id++, new()
                    {
                        Name  = new($"Components ({archetype.TypesCount})"),
                        Value = new($"{archetype.InstanceSize} B"),
                        Class = CompHeader,
                        Hash  = stableHash
                    }, comps));

                    items.Add(new(id++, new()
                    {
                        Hash = stableHash
                    }));
                    items.Add(new(id++, new()
                    {
                        Name  = counts.MaxStr,
                        Value = counts.MaxVal,
                        Class = GraphicMax,
                        Super = GraphicLabel,
                        Hash  = stableHash
                    }));
                    items.Add(new(id++, new()
                    {
                        Name  = new("0"),
                        Value = new("0"),
                        Class = GraphicMin,
                        Super = GraphicLabel,
                        Hash  = stableHash
                    }));
                    items.Add(new(id++, new()
                    {
                        // * Disgusting. But I don't want to use another label for this.
                        Name  = new("1                     Chunk Utilization"),
                        Value = new($"{archetype.ChunkCapacity}"),
                        Class = GraphicAxis,
                        Super = GraphicLabel,
                        Hash  = stableHash
                    }));
                    items.Add(new(id++, new()
                    {
                        Class  = GraphicHeader,
                        Counts = counts,
                        Hash   = stableHash
                    }));
                }
            }

            // * Update the max values for graph.
            foreach (ArchCounts counts in seen.Values)
            {
                int max = counts.Bins.Max();
                counts.MaxStr.Append($"{max,3}");
                counts.MaxVal.Append($"{max,-3}");
                counts.MaxCount = max;
                counts.CntStr.Append(counts.TotalCount);
            }

            _tree.SetRootItems(items);
            _tree.RefreshItems();
        }
    }
}
