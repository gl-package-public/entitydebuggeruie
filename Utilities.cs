﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace EntityDebuggerUIE
{
    /// <summary>
    ///     Class containing the targeted query, the entity manager that created it, and a record of the last order version.
    /// </summary>
    public class ListQuery
    {
        public EntityQuery   Query;
        public EntityManager Manager;

        private int _order;

        public ListQuery(EntityQuery query, EntityManager manager)
        {
            Query   = query;
            Manager = manager;
            _order  = -1; // * Force at least one refresh check.
        }

        public bool HasNotChanged()
        {
            var noChange = true;
            try
            {
                noChange = _order == Manager.EntityOrderVersion;
                _order   = Manager.EntityOrderVersion;
            }
            catch (Exception)
            {
                Debug.Log("Error");
                // * Manager may not exist following play mode change.
                // ? Force a world refresh.
                EntityDebuggerV2.Instance.WorldChanged = true;
            }

            return noChange;
        }
    }

    public static class Utilities
    {
        /// <summary>
        ///     Gets all queries related to currently selected system.
        /// </summary>
        /// <remarks>If selected is null, this is just returns a universal query.</remarks>
        /// <param name="queries">Output list of relevant queries.</param>
        /// <param name="settings">Component filters being applied to output queries.</param>
        public static unsafe void ResetQueries(List<ListQuery> queries, in EntitySet settings)
        {
            // * Check if all entities system is selected.
            if (EntityDebuggerV2.SelectedSystem is null)
            {
                UniversalQueries(queries, settings);
                return;
            }

            ClearQueries(queries);

            // * Pulling queries from the selected system.
            SystemState* statePtr = EntityDebuggerV2.SelectedSystem.SystemProxy.StatePointerForQueryResults;
            if (statePtr is null)
                return;

            foreach (EntityQuery query in statePtr->EntityQueries)
                queries.Add(new(query, statePtr->EntityManager));
        }

        /// <summary>
        ///     Checks if world should be included in debugger lists.
        /// </summary>
        /// <param name="w">World to check.</param>
        /// <returns>True if this world should be included.</returns>
        public static bool WorldFilter(World w)
        {
            return EntityDebuggerV2.Instance.ShowAllWorlds
                   || (w.Flags & (WorldFlags.Streaming | WorldFlags.Shadow)) == 0;
        }

        /// <summary>
        ///     Iterate through all queries to check if any had structural changes.
        /// </summary>
        /// <param name="queries">List of relevant queries.</param>
        /// <returns>True if all in list have had NO structural changes since last time checked.</returns>
        public static bool DidNotChange(in List<ListQuery> queries)
        {
            var noChange = true;
            foreach (ListQuery item in queries)
                noChange = noChange && item.HasNotChanged();

            return noChange;
        }

        /// <summary>
        ///     Generates universal queries for selected or all worlds based of entity settings.
        /// </summary>
        /// <param name="queries">Output list of relevant universal queries.</param>
        /// <param name="settings">Component filters being applied to output queries.</param>
        private static void UniversalQueries(List<ListQuery> queries, in EntitySet settings)
        {
            ClearQueries(queries);

            // * Create query for listing.
            World world = EntityDebuggerV2.SelectedWorld;
            if (world is null)
                // * Player loop selected. Show all worlds.
                foreach (World target in World.All)
                {
                    if (!WorldFilter(target))
                        continue;

                    queries.Add(GetQuery(target, settings));
                }
            else
                // * Targeted world, only find entities for this one.
                queries.Add(GetQuery(world, settings));
        }

        /// <summary>
        ///     Disposes and clears entity queries as they're all manually generated.
        /// </summary>
        /// <param name="queries"></param>
        private static void ClearQueries(List<ListQuery> queries)
        {
            // * Manually created entity query, needs to be manually disposed.
            foreach (ListQuery query in queries)
                try
                {
                    query.Query.Dispose();
                }
                catch (Exception)
                {
                    // * Query disposal may fail if source world were disposed before this method was called.
                    // * That's fine, just skip those queries.
                }

            queries.Clear();
        }

        /// <summary>
        ///     Constructs entity queries for the given world using the settings provided.
        /// </summary>
        /// <param name="world">Target world of the query.</param>
        /// <param name="settings">Filter options to include with the query.</param>
        /// <returns>Query for all entities that passed settings filter.</returns>
        private static ListQuery GetQuery(World world, EntitySet settings)
        {
            EntityManager em = world.EntityManager;

            // * Default universal query options.
            if (settings.ShowCommands && settings.ShowScenes && settings.ShowPrefabs)
                // * Get universal query from the entity manager.
                return new(settings.ShowSystems ? em.UniversalQueryWithSystems : em.UniversalQuery, em);

            using var builder = new EntityQueryBuilder(Allocator.Temp);

            // * Make sure this will show everything.
            EntityQueryOptions options = EntityQueryOptions.IncludeDisabledEntities
                                         | EntityQueryOptions.IgnoreComponentEnabledState;

            // * Hardcoded removing the command buffer singletons.
            if (!settings.ShowCommands)
                builder.WithNone<BeginInitializationEntityCommandBufferSystem.Singleton,
                        EndInitializationEntityCommandBufferSystem.Singleton,
                        BeginSimulationEntityCommandBufferSystem.Singleton,
                        EndSimulationEntityCommandBufferSystem.Singleton,
                        BeginFixedStepSimulationEntityCommandBufferSystem.Singleton,
                        EndFixedStepSimulationEntityCommandBufferSystem.Singleton,
                        BeginVariableRateSimulationEntityCommandBufferSystem.Singleton>()
                    .WithNone<EndVariableRateSimulationEntityCommandBufferSystem.Singleton,
                        BeginPresentationEntityCommandBufferSystem.Singleton>();

            if (!settings.ShowScenes)
                builder.WithNone<SceneReference, SceneSectionData>();

            if (settings.ShowSystems)
                options |= EntityQueryOptions.IncludeSystems;

            if (settings.ShowPrefabs)
                options |= EntityQueryOptions.IncludePrefab;

            // * Construct the query with structural change filter.
            return new(em.CreateEntityQuery(builder.WithOptions(options)), em);
        }
    }
}
