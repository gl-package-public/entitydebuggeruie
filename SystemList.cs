﻿using System.Collections.Generic;
using EntityDebuggerUIE.SystemElements;
using Unity.Editor.Bridge;
using Unity.Entities;
using Unity.Entities.Editor;
using Unity.Entities.UI;
using UnityEngine;
using UnityEngine.UIElements;
using TreeViewItemData = UnityEngine.UIElements.TreeViewItemData<Unity.Entities.Editor.SystemTreeViewItem>;

namespace EntityDebuggerUIE
{
    public class SystemList
    {
        /// <summary>
        ///     Change filter if the player loop changed somehow.
        /// </summary>
        private bool _graphChanged;

        private readonly MultiColumnTreeView   _systemView;
        private readonly PlayerLoopSystemGraph _playerGraph;

        /// <summary>
        ///     List of all system list values that need to be refreshed every update.
        /// </summary>
        private readonly List<ISystemRefresh> _sysValues = new();

        /// <summary>
        ///     Stores all acquired system list items for releasing when rebuilding.
        /// </summary>
        /// <remarks>
        ///     Releasing each item recursively releases its children as well.
        /// </remarks>
        private readonly List<TreeViewItemData> _items = new();

        /// <summary>
        ///     List of second depth elements for total time computation.
        /// </summary>
        /// <remarks>
        ///     Root items are not timed as they're mono behaviors.
        ///     E.g. we get `SimulationSystemGroup` in `Update` instead.
        /// </remarks>
        private readonly List<SystemTreeViewItem> _sysTimedItems = new();

        /// <summary>
        ///     Quicker access to EntityDebugger selected system property.
        /// </summary>
        private static SystemTreeViewItem Selected
        {
            get => EntityDebuggerV2.SelectedSystem;
            set => EntityDebuggerV2.SelectedSystem = value;
        }

        // * USS Classes.
        private const string Enabled = "system-enabled", Mixed = "system-mixed", Disabled = "system-disabled";

        // * System name icons.
        private const string EcbBegin = "system-icon-ecb-begin",
            EcbEnd                    = "system-icon-ecb-end",
            Unmanaged                 = "system-icon-unmanaged",
            SystemGroup               = "system-icon-group",
            SystemBase                = "system-icon-base";

        public SystemList(MultiColumnTreeView sysView, SystemSet sysSet)
        {
            _systemView = sysView;
            _playerGraph = new()
            {
                WorldProxyManager = EntityDebuggerV2.ProxyManager
            };

            // ! HARDCODED! Ensure matches with the UXML file.
            Column enabled = _systemView.columns["Enabled"];
            Column systems = _systemView.columns["Systems"];
            Column count   = _systemView.columns["Count"];
            Column timing  = _systemView.columns["Timing"];

            enabled.makeCell = () => new SystemButton();
            enabled.bindCell = (element, i) =>
            {
                var label = (SystemButton)element;
                var item  = _systemView.GetItemDataForIndex<SystemTreeViewItem>(i);

                // * There's no enabled button for all entities or mono behaviors so skip.
                // ? Null is default all entities selection.
                if (item is null || item.Node is IPlayerLoopSystemData)
                {
                    label.SetVisibility(false);
                    return;
                }

                // * Reset visibility for regular items.
                label.SetVisibility(true);

                label.Refresh = () =>
                {
                    // * Clear previous icons.
                    label.RemoveFromClassList(Enabled);
                    label.RemoveFromClassList(Mixed);
                    label.RemoveFromClassList(Disabled);

                    // * Select proper state icon.
                    // ? UI Builder throws an error for not finding these icons but they're there.
                    switch (item.GetSystemToggleState())
                    {
                        case SystemTreeViewItem.SystemToggleState.AllEnabled:
                            label.AddToClassList(Enabled);
                            return;
                        case SystemTreeViewItem.SystemToggleState.Mixed:
                            label.AddToClassList(Mixed);
                            return;
                        case SystemTreeViewItem.SystemToggleState.Disabled:
                            label.AddToClassList(Disabled);
                            return;
                    }
                };

                // * Conduct at least one refresh.
                label.Refresh();

                // * Do not continue refreshing if invalid or a mono system.
                if (CheckNoRefreshItem(item))
                    return;

                // * Store a single clickable event for removing when unbinding.
                label.Click = () =>
                {
                    SystemTreeViewItem.SystemToggleState state = item.GetSystemToggleState();

                    // * Set enabled if system is disabled. Otherwise, if mixed or enabled, set disabled.
                    item.SetSystemEnabled(state == SystemTreeViewItem.SystemToggleState.Disabled);

                    // * Force a refresh to reduce latency from click to color changes.
                    EntityDebuggerV2.Instance.ForceRefresh = true;
                };

                // * Register clicking event.
                label.clicked += label.Click;

                // * Register label for updating.
                _sysValues.Add(label);
                label.Created = true;
            };
            enabled.unbindCell = (element, _) =>
            {
                var label = (SystemButton)element;

                if (!label.Created)
                    return;

                _sysValues.Remove(label);
                label.Created =  false;
                label.clicked -= label.Click;
            };

            systems.makeCell = () => new SystemName();
            systems.bindCell = (element, i) =>
            {
                var label = (SystemName)element;
                var item  = _systemView.GetItemDataForIndex<SystemTreeViewItem>(i);

                // * Null is default all entities selection.
                // ? Custom hardcoded name for this.
                if (item is null)
                {
                    string name = EntityDebuggerV2.SelectedWorld is null
                        ? "All Worlds"
                        : EntityDebuggerV2.SelectedWorld.Name;
                    label.Text = $"All Entities ({name})";

                    label.Style.unityFontStyleAndWeight = FontStyle.Bold;

                    label.SetEnabled(true);

                    // * No need to refresh this, the name will not change without a world rebuild.
                    return;
                }

                // * Primary information. System names do not change.
                label.Text = item.GetSystemName();

                // * Systems that have children must be a system group.
                label.Style.unityFontStyleAndWeight = item.HasChildren ? FontStyle.Bold : FontStyle.Normal;

                // * And add the system icon. System classes do not change.
                label.AddToClassList(GetSystemClass(item.SystemProxy));

                label.Refresh = () =>
                {
                    // * Leaf systems that are not valid are from mono land.
                    label.SetEnabled(IsValidSystemOrGroup(item) && item.GetParentState());
                };

                // * Conduct at least one refresh.
                label.Refresh();

                // * Do not continue refreshing if invalid or a mono system.
                if (CheckNoRefreshItem(item))
                    return;

                _sysValues.Add(label);
                label.Created = true;
            };
            systems.unbindCell = (element, _) =>
            {
                var label = (SystemName)element;

                // * Remove old icons.
                label.RemoveFromClassList(EcbBegin);
                label.RemoveFromClassList(EcbEnd);
                label.RemoveFromClassList(Unmanaged);
                label.RemoveFromClassList(SystemBase);
                label.RemoveFromClassList(SystemGroup);

                if (!label.Created)
                    return;

                _sysValues.Remove(label);
                label.Created = false;
            };

            count.makeCell = () => new SystemValue();
            count.bindCell = (element, i) =>
            {
                var label = (SystemValue)element;
                var item  = _systemView.GetItemDataForIndex<SystemTreeViewItem>(i);

                // * Special hardcoded all entities selection.
                if (item is null)
                {
                    if (EntityDebuggerV2.SelectedWorld is null)
                    {
                        // * Not counting full player loop.
                        label.text = Constants.SystemSchedule.k_Dash;
                        return;
                    }

                    label.SetVisibility(true);
                    label.SetEnabled(true);

                    label.Refresh = () =>
                    {
                        // * Get count of all non-system entities.
                        label.text = EntityDebuggerV2.SelectedWorld.EntityManager
                            .UniversalQuery.CalculateEntityCount().ToString();
                    };
                }
                else
                {
                    // * Regular system list item.
                    label.Refresh = () =>
                    {
                        string matches = item.GetEntityMatches();
                        label.text = matches;

                        // * Mono-behaviors have no entities.
                        // ? Check is done by the view item for us.
                        label.SetVisibility(!string.IsNullOrEmpty(matches));

                        // * Shading for enabled state.
                        label.SetEnabled(item.GetParentState());
                    };
                }

                // * Conduct at least one refresh.
                label.Refresh();

                // * Do not continue refreshing if invalid or a mono system.
                if (CheckNoRefreshItem(item))
                    return;

                _sysValues.Add(label);
                label.Created = true;
            };
            count.unbindCell = (element, _) =>
            {
                var label = (SystemValue)element;

                if (!label.Created)
                    return;

                _sysValues.Remove(label);
                label.Created = false;
            };

            timing.makeCell = () => new SystemValue();
            timing.bindCell = (element, i) =>
            {
                var label = (SystemValue)element;
                var item  = _systemView.GetItemDataForIndex<SystemTreeViewItem>(i);

                if (item is null)
                {
                    // * Always visible.
                    label.SetVisibility(true);

                    // * Always enabled.
                    label.SetEnabled(true);

                    // * Disgusting string interpolation but I'm too lazy to get the numbers properly.
                    label.Refresh = () =>
                    {
                        var total = 0.0f;
                        foreach (SystemTreeViewItem timedItem in _sysTimedItems)
                        {
                            if (!float.TryParse(timedItem.GetRunningTime(sysSet.MorePrecision), out float time))
                                continue;

                            total += time;
                        }

                        // * The AdjustPrecision() inside GetRunningTime().
                        label.text = total.ToString(sysSet.MorePrecision ? "f4" : "f2");
                    };
                }
                else
                {
                    // * Regular system group.
                    label.Refresh = () =>
                    {
                        string time = item.GetRunningTime(sysSet.MorePrecision);
                        label.text = time;

                        // * Mono-behaviors have no entities.
                        // ? Check is done by the view item for us.
                        label.SetVisibility(!string.IsNullOrEmpty(time));

                        // * Shading for enabled state.
                        label.SetEnabled(item.GetParentState());
                    };
                }

                // * Conduct at least one refresh.
                label.Refresh();

                // * Do not continue refreshing if invalid or a mono system.
                if (CheckNoRefreshItem(item))
                    return;

                _sysValues.Add(label);
                label.Created = true;
            };
            timing.unbindCell = (element, _) =>
            {
                var label = (SystemValue)element;

                if (!label.Created)
                    return;

                _sysValues.Remove(label);
                label.Created = false;
            };

            _systemView.selectionChanged += _ =>
            {
                // * Returns either the selected system or null if selection cleared.
                Selected = _systemView.selectedItem as SystemTreeViewItem;

                // * Reset the archetype selection as well.
                EntityDebuggerV2.SelectedArchHash = 0;

                if (Selected is null || CheckNoRefreshItem(Selected))
                    return;

                SelectionUtility.ShowInInspector(new SystemContentProvider
                {
                    World            = Selected.SystemProxy.World,
                    SystemProxy      = Selected.SystemProxy,
                    LocalSystemGraph = _playerGraph
                }, new()
                {
                    UseDefaultMargins     = false,
                    ApplyInspectorStyling = false
                });

                // * Force a redraw as well.
                EntityDebuggerV2.Instance.ForceRefresh = true;
            };
        }

        /// <summary>
        ///     Checks if given this item should the row refresh.
        /// </summary>
        /// <param name="item">Item used to generate row data.</param>
        /// <returns>True if should NOT refresh. False if should refresh.</returns>
        private static bool CheckNoRefreshItem(SystemTreeViewItem item)
        {
            // * Null item is show all entities option and should always update.
            return item is not null && (!item.SystemProxy.Valid || item.SystemProxy.World is null);
        }

        /// <summary>
        ///     Checks if this item is a valid entity system.
        /// </summary>
        /// <param name="item">Item used to generate row data.</param>
        /// <returns>True if its an entity system. False if its a mono behavior.</returns>
        private static bool IsValidSystemOrGroup(SystemTreeViewItem item)
        {
            return item.SystemProxy.Valid || item.HasChildren;
        }

        /// <summary>
        ///     Gets the CSS class for the icon before a system name.
        /// </summary>
        /// <param name="systemProxy">System proxy from the item used to generate this row.</param>
        /// <returns>CSS containing type icon texture.</returns>
        private static string GetSystemClass(SystemProxy systemProxy)
        {
            if (!systemProxy.Valid)
                return string.Empty;

            if ((systemProxy.Category & SystemCategory.ECBSystemBegin) != 0)
                return EcbBegin;
            if ((systemProxy.Category & SystemCategory.ECBSystemEnd) != 0)
                return EcbEnd;
            if ((systemProxy.Category & SystemCategory.EntityCommandBufferSystem) != 0)
                return string.Empty;
            if ((systemProxy.Category & SystemCategory.Unmanaged) != 0)
                return Unmanaged;
            if ((systemProxy.Category & SystemCategory.SystemGroup) != 0)
                return SystemGroup;
            if ((systemProxy.Category & SystemCategory.SystemBase) != 0)
                return SystemBase;

            return string.Empty;
        }

        /// <summary>
        ///     System change checker to see if the data behind the list needs to be rebuilt.
        /// </summary>
        public void Update()
        {
            foreach (IWorldProxyUpdater updater in EntityDebuggerV2.ProxyManager.GetAllWorldProxyUpdaters())
            {
                if (!updater.IsActive() || !updater.IsDirty())
                    continue;

                _graphChanged = true;
                updater.SetClean();
            }

            if (_graphChanged)
                _playerGraph.BuildCurrentGraph();

            if (_graphChanged || EntityDebuggerV2.Instance.WorldChanged)
            {
                RebuildSystemList();
                Selected = null;
            }

            // * Finally update all the system elements with new data.
            foreach (ISystemRefresh value in _sysValues)
                value.Refresh();

            _graphChanged = false;
        }

        /// <summary>
        ///     Very expensive system to completely rebuild the node tree used to create the system list.
        /// </summary>
        private void RebuildSystemList()
        {
            //Debug.Log("Rebuilding system list.");

            // * Release children back into the pool.
            foreach (TreeViewItemData item in _items)
                item.data?.Release();

            // * Clear the list trackers.
            _items.Clear();
            _sysTimedItems.Clear();
            _sysValues.Clear();

            if (World.All.Count == 0)
            {
                // * No world, set to empty list and clear.
                _systemView.SetRootItems(_items);
                _systemView.Rebuild();
                return;
            }

            var id = 0;
            // * Add the default null option to view all entities at the top of the list.
            _items.Add(new(id++, null));

            foreach (IPlayerLoopNode node in _playerGraph.Roots)
            {
                if (!node.ShowForWorldProxy(EntityDebuggerV2.SelectedProxy))
                    continue;

                SystemTreeViewItem item = SystemTreeViewItem.Acquire(_playerGraph, node, null,
                    EntityDebuggerV2.SelectedProxy);

                if (item is null)
                    continue;

                // * Next recursion list.
                var branches = new List<TreeViewItemData>();

                // * Pass this section of systems to the UI for rendering.
                _items.Add(new(id++, item, branches));

                // ! In the future, cache a system name dictionary to construct a string tree for searching.
                // ? Look at BuildSystemDependencyMap() inside SystemTreeView for why and how.
                PopulateChildren(item, branches, ref id);

                // * Pull out the second depth nodes for total time calculation.
                foreach (TreeViewItemData second in branches)
                {
                    // * Skip the mono-behaviors.
                    if (second.data.Node is IPlayerLoopSystemData)
                        continue;

                    _sysTimedItems.Add(second.data);
                }
            }

            _systemView.SetRootItems(_items);
            _systemView.Rebuild();
            _systemView.ExpandAll();
        }

        /// <summary>
        ///     Recursive method to completely construct a system node's tree.
        /// </summary>
        private static void PopulateChildren(SystemTreeViewItem item, ICollection<TreeViewItemData> branches,
            ref int id)
        {
            if (!item.HasChildren)
                return;

            item.PopulateChildren();

            foreach (ITreeViewItem iChild in item.children)
            {
                // * Also checks for null.
                if (iChild is not SystemTreeViewItem child)
                    continue;

                var leaf = new List<TreeViewItemData>();
                branches.Add(new(id++, child, leaf));

                PopulateChildren(child, leaf, ref id);
            }
        }
    }
}
