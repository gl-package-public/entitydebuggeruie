using System;
using Unity.Entities;
using Unity.Entities.Editor;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using TreeViewItemData = UnityEngine.UIElements.TreeViewItemData<Unity.Entities.Editor.SystemTreeViewItem>;

namespace EntityDebuggerUIE
{
    internal class EntityDebuggerV2 : EditorWindow
    {
        public static EntityDebuggerV2 Instance;

        /// <summary>
        ///     Display loading and hidden worlds.
        /// </summary>
        internal bool ShowAllWorlds;

        /// <summary>
        ///     Change booleans.
        /// </summary>
        internal bool ForceRefresh,
            WorldChanged,
            EntityChanged,
            EntListChanged,
            ArchChanged,
            ChunkChanged,
            SystemChanged;

        [SerializeField] private VisualTreeAsset window;

        // * Visual elements.
        private WorldPopup _worldPopup;
        private ResizePane _resizePane;
        private SystemSet  _systemSet;
        private SystemList _systemList;
        private EntitySet  _entitySet;
        private EntityList _entityList;
        private Label      _entityCnt;
        private TreeView   _chunkView;
        private ChunkList  _chunkList;

        // * Buttons for windows in the entity view.
        private ToolbarToggle _infoToggle;

        // * Separated from above because of how verbose the name is.
        private MultiColumnTreeView _systemView, _entityView;

        /// <summary>
        ///     Throttles the update frequency so it doesn't lag everything.
        /// </summary>
        private readonly Cooldown _cooldown = new(TimeSpan.FromMilliseconds(Constants.Inspector.CoolDownTime));

        /// <summary>
        ///     Backing data for ProxyManager.
        /// </summary>
        private WorldProxyManager _proxyManager;

        /// <summary>
        ///     Backing data for SelectedSystem.
        /// </summary>
        private SystemTreeViewItem _systemItem;

        /// <summary>
        ///     Backing data for SelectedWorld.
        /// </summary>
        private World _world;

        /// <summary>
        ///     Storing proxy because ProxyManager's version can not be null.
        /// </summary>
        private WorldProxy _worldProxy;

        /// <summary>
        ///     Backing data for SelectedEntity.
        /// </summary>
        private EntitySelectionProxy _entityProxy;

        /// <summary>
        ///     Backing data of archetype hash.
        /// </summary>
        private ulong _archetypeHash, _selectedChunk;

        /// <summary>
        ///     Stable hash of currently selected archetype.
        /// </summary>
        public static ulong SelectedArchHash
        {
            get => Instance._archetypeHash;
            set
            {
                // * Deselect on double selection.
                Instance._archetypeHash = Instance._archetypeHash == value ? 0 : value;
                Instance.ArchChanged    = true;
            }
        }

        /// <summary>
        ///     Stable hash of chunk selected inside the entities list.
        /// </summary>
        public static ulong SelectedChunkHash
        {
            get => Instance._selectedChunk;
            set
            {
                // * Change check.
                Instance.ChunkChanged   = Instance._selectedChunk != value;
                Instance._selectedChunk = value;
            }
        }

        public static WorldProxyManager ProxyManager
        {
            get => Instance._proxyManager;
            private set => Instance._proxyManager = value;
        }

        /// <summary>
        ///     Currently targeted system in the left side system list.
        /// </summary>
        public static SystemTreeViewItem SelectedSystem
        {
            get => Instance._systemItem;
            set
            {
                Instance.SystemChanged = Instance._systemItem != value;

                if (Instance.SystemChanged)
                    Instance._systemItem = value;
            }
        }

        /// <summary>
        ///     Currently targeted entity world.
        /// </summary>
        /// <remarks>
        ///     Null indicates showing full player loop.
        /// </remarks>
        public static World SelectedWorld
        {
            get => Instance._world;
            set
            {
                SelectedProxy = value is null ? null : ProxyManager.GetWorldProxyForGivenWorld(value);

                Instance.WorldChanged = Instance._world != value;

                // * Reset selected chunk archetype on world change.
                if (Instance.WorldChanged)
                    SelectedArchHash = 0;

                Instance._world = value;
            }
        }

        /// <summary>
        ///     Currently targeted entity world proxy.
        /// </summary>
        /// <remarks>
        ///     Null indicates showing full player loop.
        /// </remarks>
        public static WorldProxy SelectedProxy
        {
            get => Instance._worldProxy;
            private set
            {
                Instance._worldProxy  = value;

                // * Proxy manager can not have a null assigned to it.
                if (value is null)
                    return;

                // * Then assign the manager's targeted proxy.
                ProxyManager.SelectedWorldProxy = value;
            }
        }

        public static EntitySelectionProxy SelectedEntity
        {
            get => Instance._entityProxy;
            set
            {
                Instance._entityProxy  = value;
                Instance.EntityChanged = Instance._entityProxy.Entity != value.Entity;
            }
        }

        [MenuItem("Window/Entities/Entity Debugger V2", false)]
        public static void OpenWindow()
        {
            // ! Enter the update process here!
            var wnd = GetWindow<EntityDebuggerV2>();
            wnd.titleContent = new("Entity Debugger V2");
        }

        private void OnSelectionChange()
        {
            if (Selection.activeObject is EntitySelectionProxy { Exists: true } selectionProxy)
            {
                // * Someone somewhere selected an entity.
                SelectedEntity = selectionProxy;

                // * Only switch worlds if not already showing it.
                if (SelectedWorld is not null)
                {
                    SelectedWorld = selectionProxy.World;

                    // * Manual update to redraw everything now that world changed.
                    ForceRefresh = true;
                    Update();
                }

                _entityList.SetSelection();
            }
        }

        private void CreateGUI()
        {
            if (Instance != null)
                return;

            Instance = this;

            ProxyManager = new();
            ProxyManager.CreateWorldProxiesForAllWorlds();

            // * Instantiate UXML
            VisualElement labelFromUxml = window.Instantiate();
            rootVisualElement.Add(labelFromUxml);

            // * Pull references to all sub-windows that need updating.
            _resizePane = rootVisualElement.Q<ResizePane>();
            _worldPopup = rootVisualElement.Q<WorldPopup>();
            _systemSet  = rootVisualElement.Q<SystemSet>();
            _systemView = rootVisualElement.Q<MultiColumnTreeView>("SystemList");
            _systemList = new(_systemView, _systemSet);
            _entitySet  = rootVisualElement.Q<EntitySet>();
            _entityView = rootVisualElement.Q<MultiColumnTreeView>("Hierarchy");
            _entityCnt  = rootVisualElement.Q<Label>("EntityCount");
            _entityList = new(_entityView, _entitySet, _entityCnt);
            _infoToggle = rootVisualElement.Q<ToolbarToggle>("ChunkInfoToggle");
            _chunkView  = rootVisualElement.Q<TreeView>("ChunkInfo");
            _chunkList  = new(_chunkView, _entitySet);

            // * Set up the cursor.
            _resizePane.Create();
        }

        private void Update()
        {
            // * Always update the sub-window display existence.
            _chunkView?.SetVisibility(_infoToggle.value);

            if (!ForceRefresh && !_cooldown.Update(DateTime.Now))
                return;

            // if (_worldPopup is null || _systemSet is null || _systemList is null
            //     || _entitySet is null || _entityList is null || _chunkList is null)
            //     return;

            // * Main update loop.
            _worldPopup.Update();
            _systemSet.Update(_systemView);
            _systemList.Update();
            _entitySet.Update();
            _entityList.Update();
            _chunkList.Update();

            // * Clear triggers now that update should've consumed them.
            ForceRefresh = WorldChanged = EntityChanged = EntListChanged = ArchChanged = ChunkChanged = SystemChanged
                = false;
        }

        private void OnDestroy()
        {
            // * Clear out the static reference to allow for recreation.
            Instance = null;
        }
    }
}
