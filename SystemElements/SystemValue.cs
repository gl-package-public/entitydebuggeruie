﻿using System;
using UnityEngine.UIElements;

namespace EntityDebuggerUIE.SystemElements
{
    public class SystemValue : Label, ISystemRefresh
    {
        public Action Refresh { get; set; }

        public bool Created;
    }
}
