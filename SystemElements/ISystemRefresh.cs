﻿using System;

namespace EntityDebuggerUIE.SystemElements
{
    public interface ISystemRefresh
    {
        public Action Refresh { get; set; }
    }
}
