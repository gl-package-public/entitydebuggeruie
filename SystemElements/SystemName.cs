﻿using System;
using UnityEngine.UIElements;

namespace EntityDebuggerUIE.SystemElements
{
    public class SystemName : VisualElement, ISystemRefresh
    {
        public Action Refresh { get; set; }

        public bool Created;

        private readonly Label _text;

        public string Text
        {
            set => _text.text = value;
        }

        public IStyle Style => _text.style;

        public SystemName()
        {
            // * Add required elements.
            var icon = new VisualElement();
            icon.AddToClassList("system-icon");
            Add(icon);

            _text = new();
            Add(_text);
        }
    }
}
