﻿using System;
using UnityEngine.UIElements;

namespace EntityDebuggerUIE.SystemElements
{
    public class SystemButton : Button, ISystemRefresh
    {
        public Action Refresh { get; set; }

        public bool Created;

        public Action Click;
    }
}
