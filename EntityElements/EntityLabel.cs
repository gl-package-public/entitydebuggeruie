﻿using UnityEngine.UIElements;

namespace EntityDebuggerUIE.EntityElements
{
    public class EntityLabel : VisualElement
    {
        private readonly Label _text;

        public string Text
        {
            set => _text.text = value;
        }

        public EntityLabel()
        {
            AddToClassList("entity-label");

            var icon = new VisualElement();
            icon.AddToClassList("entity-icon");

            Add(icon);

            _text = new();
            Add(_text);
        }
    }
}
