﻿using System;
using UnityEngine.UIElements;

namespace EntityDebuggerUIE.EntityElements
{
    public class ChunkItem : VisualElement
    {
        public readonly Label Name, Value;

        public readonly ChunkGraph Graph;

        public Action Refresh;

        public ChunkItem()
        {
            VisualElement icon = new();
            Add(icon);
            icon.AddToClassList("chunk-item-icon");

            Name = new();
            Add(Name);
            Name.AddToClassList("chunk-item-name");

            Value = new();
            Add(Value);
            Value.AddToClassList("chunk-item-value");

            Graph = new();
            Add(Graph);
            Graph.AddToClassList("chunk-item-graph");
        }
    }
}
