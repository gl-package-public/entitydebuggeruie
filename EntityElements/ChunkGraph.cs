﻿using UnityEngine.UIElements;

namespace EntityDebuggerUIE.EntityElements
{
    public class ChunkGraph : VisualElement
    {
        public ChunkList.ArchCounts Counts;

        /// <summary>
        ///     Hardcoded pixel dimensions for the graph from UXML!
        /// </summary>
        private const float Height = 64, Width = 205f;

        public ChunkGraph()
        {
            generateVisualContent += OnGenerateVisualContent;
        }

        private void OnGenerateVisualContent(MeshGenerationContext mgc)
        {
            if (Counts is null)
                return;

            Painter2D painter = mgc.painter2D;

            painter.fillColor = new(1f, 0.6f, 0f);

            // * Always greater than 1 because max entities per chunk is hardcoded at 128.
            float step = Width / Counts.Bins.Length;

            painter.BeginPath();
            // * Moving to the bottom left hand corner.
            painter.MoveTo(new(0, Height));
            for (int i = 0, j = 1; i < Counts.Bins.Length; i++, j++)
            {
                // * Skipping Bins[0] because empty chunks are not reported.
                float height = (1f - Counts.Bins[i] / Counts.MaxCount) * Height;
                // * Move "up" (view is inverted so moving down Y) to relative position.
                painter.LineTo(new(i * step, height));
                // * Cap off the histogram.
                painter.LineTo(new(j * step, height));
            }

            // * End the last column.
            painter.LineTo(new(Counts.Bins.Length * step, Height));

            painter.Fill();
        }
    }
}
